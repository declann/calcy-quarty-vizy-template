// soon: rewrite on NAND logic

export const A = () => A_in;
export const B = () => B_in;

export const AND = () => A() && B();

export const NAND = () => !AND(); // not and

export const OR = () => A() || B();

export const XOR = () => {
  // exclusive or
  if (AND()) return 0;
  else return OR();
};
