import { XOR } from "./cul_scope_0.mjs";import { OR } from "./cul_scope_0.mjs";import { NAND } from "./cul_scope_0.mjs";import { AND } from "./cul_scope_0.mjs";import { B } from "./cul_scope_0.mjs";import { A } from "./cul_scope_0.mjs"; // soon: rewrite on NAND logic

export const A_ = ({ A_in }) => A_in;
export const B_ = ({ B_in }) => B_in;

export const AND_ = ({ A_in, B_in }) => A({ A_in }) && B({ B_in });

export const NAND_ = ({ A_in, B_in }) => !AND({ A_in, B_in }); // not and

export const OR_ = ({ A_in, B_in }) => A({ A_in }) || B({ B_in });

export const XOR_ = ({ A_in, B_in }) => {
  // exclusive or
  if (AND({ A_in, B_in })) return 0;else
  return OR({ A_in, B_in });
};