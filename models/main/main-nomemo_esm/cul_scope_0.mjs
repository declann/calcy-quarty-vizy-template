// soon: rewrite on NAND logic

export const A = ({ A_in }) => A_in;
export const B = ({ B_in }) => B_in;

export const AND = ({ A_in, B_in }) => A({ A_in }) && B({ B_in });

export const NAND = ({ A_in, B_in }) => !AND({ A_in, B_in }); // not and

export const OR = ({ A_in, B_in }) => A({ A_in }) || B({ B_in });

export const XOR = ({ A_in, B_in }) => {
  // exclusive or
  if (AND({ A_in, B_in })) return 0;else
  return OR({ A_in, B_in });
};