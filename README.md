# calcy-quarty-vizy-template

This a quick and rough **template for developers**.

This template - and calculang itself - is under development.

There is [a video](https://www.youtube.com/watch?v=bDxJTlvqzPs) demonstrating a development workflow using this template.

## How to use

*Note: don't install or run commands from a source you do not trust and/or have not reviewed.*

Install [quarto](https://quarto.org) (required for `preview` scripts). I'm currently using the v1.4 pre-releases.

Clone this repo, then to install other dependencies including the calculang compiler run:

```shell
npm install
```

Now package scripts should work.

### calculang model development

**You can develop a calculang model in `./models/main/main.cul.js`** and then run the following scripts:

To compile with memoization:

```shell
npm run main:compile-memo
```

or without:

```shell
npm run main:compile-nomemo
```

The calculang compiler has shamefully minimal error-reporting now; but it's useful to pay attention to eslint output on `.mjs` outputs in `_esm` folders (only).

### Quarto pages development

Besides `.cul.js` files in `./models`, you will also need to develop the Quarto pages in `./pages`. I use Quarto in this template because it is robust and supports [Observable JS](https://observablehq.com/@observablehq/observables-not-javascript), this gives a lot of flexibility for rapid development. Quarto also supports running Python, R and Julia.

Quarto [documentation](https://quarto.org/docs/guide/) is excellent; I recommend beginning with [their Observable JS resources](https://quarto.org/docs/computations/ojs). ObservableHQ documentation is also great, for example on [inputs](https://observablehq.com/@observablehq/inputs).

You can run the following commands to open a preview of the model pages:

```shell
npm run preview # runs `quarto preview`
```

**Pages will refresh automatically after new compilation scripts are re-run.** 🪄 (Related: #6+)

## Credits 💓

The template pulls together a mix of tools and the name includes more or less subtle hat-tips.

- [calculang](https://github.com/calculang/calculang), a language for calculations
- [Quarto](https://quarto.org), a scientific and technical publishing system
- a visualisation/interaction stack, notably including:
  - [vega-lite](https://vega.github.io/vega-lite/) visualization grammar (a language for visualization?)
  - [CompassQL](https://github.com/vega/compassql) visualization query language
  - ~~[gemini](https://github.com/uwdata/gemini) for animated transitions~~ (will make a comeback)
  - [Observable runtime](https://github.com/observablehq/runtime) (via Quarto)
  - Above tools are from [University of Washington Interactive Data Lab](https://idl.cs.washington.edu/) and [ObservableHQ](https://observablehq.com/)
  - [Graphviz](https://graphviz.org/) graph visualization

(This is not a complete list of credits)

## Resources & Privacy

Generated webpages may pull resources from CDNs, because this is Quartos default behaviour.

There are privacy and compliance, performance, network and reliability implications, which are especially important to consider if you publish these webpages.

For my own websites and development process, I have workarounds. Tell [me](https://calcwithdec.dev/about.html) if you need this.

## Data Privacy

It's useful in my development work to use the `...` button on visuals and `Open in Vega Editor`. **This sends model results to an externally controlled Vega Editor service.**

## Publishing & Community Gallery 🖼️

For now, [talk to me](https://calcwithdec.dev/about.html) if you have questions or issues around publishing models; I am *inclined to be interested* to support developers who will contribute to a **community gallery of models** and interaction tools.

## License & Warranty

- [LICENSE](./LICENSE)
- There is no warranty; per the license. For support please use [public channels](https://gitlab.com/declann/calcy-quarty-vizy-template/-/issues) or [get in touch](https://calcwithdec.dev/about.html).
